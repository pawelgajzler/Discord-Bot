﻿namespace Discord_Bot.CommonTypes
{
    public class Constants
    {
        #region Commands
        public const string DummyCommand = "goszmaciura";
        public const string StartRaffleCommand = "startraffle";
        public const string JoinRaffleCommand = "raffle";
        #endregion

        #region Songrequest Constants
        public const string PathToTmpFile = "C:\\\\Temp\\songrequestFile.mp3";
        public const string PathToFFMpeg = "C:\\\\Temp\\ffmpeg.exe";
        public const string VideoInfoUrl = "https://youtube.com/get_video_info?video_id=";
        public const string YoutubeUrl = "https://www.youtube.com/watch?";
        #endregion

        #region Messages
        public const string EmptyRaffle = "Nobody join to raffle :(";
        #endregion
    }
}