﻿namespace Discord_Bot.CommonTypes.Models
{
    public class SongRequest
    {
        public string Title { get; set; } = string.Empty;

        public string Link { get; set; } = string.Empty;

        public string RequestedBy { get; set; } = string.Empty;
    }
}
