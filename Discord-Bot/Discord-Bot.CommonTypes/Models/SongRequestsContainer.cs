﻿using System.Collections.Generic;

namespace Discord_Bot.CommonTypes.Models
{
    public class SongRequestsContainer
    {
        public bool IsPlayed { get; set; }
        public bool IsConnected { get; set; }
        public int LimitPerUser { get; set; }
        public List<SongRequest> SongRequests { get; set; }

        public SongRequestsContainer()
        {
            IsConnected = false;
            IsPlayed = false;
            LimitPerUser = 2;
            SongRequests = new List<SongRequest>();
        }
    }
}
