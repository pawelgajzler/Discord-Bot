﻿using System.Collections.Generic;

namespace Discord_Bot.CommonTypes.Models
{
    public class ConfigurationModel
    {
        public string BotToken { get; set; }

        public string AdminGroup { get; set; }

        public List<string> ModuleList { get; set; }

        public List<string> RestrictedSentences { get; set; }
    }
}
