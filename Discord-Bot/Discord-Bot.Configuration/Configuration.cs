﻿using System;
using System.IO;
using Discord_Bot.CommonTypes.Models;
using Newtonsoft.Json;

namespace Discord_Bot.Configuration
{
    public class ConfigurationManager
    {
        public ConfigurationModel Config { get; set; }

        #region SingletonImplementation

        private static ConfigurationManager instance;

        public ConfigurationManager(bool testConfig = false)
        {
            this.Config = testConfig ? GetConfigFromTestConfig() : GetConfigFromFile();
        }

        public static ConfigurationManager Instance
        {
            get
            {
                return instance ?? (instance = new ConfigurationManager());
            }
        }

        #endregion

        private ConfigurationModel GetConfigFromFile()
        {
            using(StreamReader reader = new StreamReader(AppContext.BaseDirectory + "/botconfig.json"))
            {
                string content = reader.ReadToEnd();
                var config = JsonConvert.DeserializeObject<ConfigurationModel>(content);
                return config;
            }
        }

        private ConfigurationModel GetConfigFromTestConfig()
        {
            using (StreamReader reader = new StreamReader(AppContext.BaseDirectory + "/testconfig.json"))
            {
                string content = reader.ReadToEnd();
                var config = JsonConvert.DeserializeObject<ConfigurationModel>(content);
                return config;
            }
        }
    }
}
