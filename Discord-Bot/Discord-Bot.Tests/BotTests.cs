using Discord_Bot.App.Helpers;
using Xunit;

namespace Discord_Bot.Tests
{
    public class BotTests
    {
        [Theory]
        [InlineData("https://www.youtube.com/watch?v=n66WbEndLhc")]
        [InlineData("v=n66WbEndLhc")]
        [InlineData("n66WbEndLhc")]
        public void TestLinkBuilder(string link)
        {
            string output = YouTubeLinkBuilder.BuildLink(link);
            Assert.Equal("https://www.youtube.com/watch?v=n66WbEndLhc", output);
        }

        [Theory]
        [InlineData("https://youtube.com/watch?v=n66WbEndLhc")]
        [InlineData("https://youtu.be/n66WbEndLhc")]
        public void TestLinkWithShortLinks(string link)
        {
            string output = YouTubeLinkBuilder.BuildLink(link);
            Assert.Equal(link, output);
        }

        [Theory]
        [InlineData("www.youtube.com/watch?v=n66WbEndLhc")]
        [InlineData("youtube.com/watch?v=n66WbEndLhc")]
        [InlineData("youtu.be/n66WbEndLhc")]
        public void TestLinkWithoutHttp(string link)
        {
            string output = YouTubeLinkBuilder.BuildLink(link);
            Assert.Equal($"https://{link}", output);
        }

        [Fact]
        public void GetVideoInfoFromCorrectLink()
        {
            string link = "https://www.youtube.com/watch?v=CnhQbhcSplk";
            string title = YoutubeDownloader.GetTitle(link);
            Assert.Equal("ADRIAN POLAK - GOLD DIGGER feat. Lil Primy", title);
        }

        [Fact]
        public void GetVideoInfoFromIncorrectLink()
        {
            string link = "https://www.youtube.com/watch?v=n66WbEndLhceqfwkamowaevoswevmf";
            string title = YoutubeDownloader.GetTitle(link);
            Assert.Equal(string.Empty, title);
        }
    }
}
