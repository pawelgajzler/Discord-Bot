﻿using Discord_Bot.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Discord_Bot.Tests
{
    public class ConfigurationClass
    {
        [Fact]
        public void GetConfigFromTest()
        {
            ConfigurationManager configurationManager = new ConfigurationManager(true);
            Assert.Equal("MjM4MzQ2NTk3NDcyMjcyMzg0.DhKbdg.OVDKmwJzYME4Jv5kERqzdJ-4YKY", configurationManager.Config.BotToken);
            Assert.Equal("Bot-Moderators", configurationManager.Config.AdminGroup);
            Assert.Equal("SongrequestModule", configurationManager.Config.ModuleList[0]);
        }
    }
}
