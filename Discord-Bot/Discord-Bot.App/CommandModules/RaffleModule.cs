﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Discord_Bot.App.CommandModules
{
    public class RaffleModule : BaseModule
    {
        List<DiscordUser> Users;
        bool isStarted = false;

        #region Commands
        [Command("startraffle")]
        public async Task Start(CommandContext context, int timeInSeconds)
        { 
            Random randomGenerator = new Random();
            Users = new List<DiscordUser>();
            isStarted = true;
            await context.RespondAsync($"New Raffle started. Type !raffle to join. Raffle ended for {timeInSeconds} seconds.", true);
            await Task.Delay(TimeSpan.FromSeconds((int)timeInSeconds));
            isStarted = false;
            if (Users.Count == 0)
            {
                await context.RespondAsync("Nobody joined to raffle :(");
            }
            DiscordUser winner = Users[randomGenerator.Next(Users.Count - 1)];
            await context.RespondAsync($"{winner.Mention} you win!!!");
        }

        [Command("raffle")]
        public async Task Join(CommandContext context)
        {
            DiscordUser joinedUser = context.User;
            if(isStarted && !Users.Where(x => x.Username == joinedUser.Username).Any())
            {
                Users.Add(joinedUser);
            }
        }
        #endregion
    }
}
