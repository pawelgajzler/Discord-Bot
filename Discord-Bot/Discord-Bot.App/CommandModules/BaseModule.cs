﻿using Discord_Bot.App.Interfaces;
using Discord_Bot.Configuration;
using DSharpPlus.CommandsNext;
using System.Linq;

namespace Discord_Bot.App.CommandModules
{
    public abstract class BaseModule : ICommandModule
    {
        protected bool UserPermittedToUse(CommandContext context, bool useTestConfig=false)
        {
            ConfigurationManager configurationManager = useTestConfig ? new ConfigurationManager(true) : ConfigurationManager.Instance;
            string permittedGroup = configurationManager.Config.AdminGroup;
            return context.Guild.IsOwner || context.Guild.CurrentMember.Roles.Where(x => x.Name == permittedGroup).Any();
        }
    }
}
