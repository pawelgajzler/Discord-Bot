﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.VoiceNext;
using DSharpPlus.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord_Bot.CommonTypes.Models;
using Discord_Bot.App.Helpers;
using System.IO;
using Discord_Bot.CommonTypes;
using System.Diagnostics;

namespace Discord_Bot.App.CommandModules
{
    public class SongrequestModule : BaseModule
    {
        private Dictionary<ulong, SongRequestsContainer> songRequestsContainers;

        public SongrequestModule()
        {
            songRequestsContainers = new Dictionary<ulong, SongRequestsContainer>();
        }

        #region Commands
        [Command("startsr")]
        public async Task StartSongRequests(CommandContext context, string name)
        {
            if (UserPermittedToUse(context))
            {
                return;
            }
            var channel = context.Guild.Channels.Where(x => x.Type == DSharpPlus.ChannelType.Voice && x.Name == name).FirstOrDefault();
            if (channel == null || channel == default(DiscordChannel))
            {
                await context.RespondAsync("Sorry but I can't find channel with this name :(");
                return;
            }
            var voiceNext = context.Client.GetVoiceNextClient();
            var voiceConnection = voiceNext.GetConnection(context.Guild);
            if(voiceConnection != null)
            {
                await context.RespondAsync("I am already connected in somewhere. First I must leave current channel.");
                return;
            }
            
            voiceConnection = await voiceNext.ConnectAsync(channel);
            if (!songRequestsContainers.ContainsKey(context.Guild.Id))
            {
                songRequestsContainers.Add(context.Guild.Id, new SongRequestsContainer());
            }
            voiceConnection = voiceNext.GetConnection(context.Guild);
            songRequestsContainers[context.Guild.Id].IsConnected = voiceConnection != null;
            if (voiceConnection != null && songRequestsContainers[context.Guild.Id].SongRequests.Count != 0)
            {
                songRequestsContainers[context.Guild.Id].IsPlayed = true;
                await Play(context);
            }
        }

        [Command("stopsr")]
        public async Task StopSongRequests(CommandContext context)
        {
            if (UserPermittedToUse(context))
            {
                return;
            }
            var voiceNext = context.Client.GetVoiceNextClient();
            var voiceConnection = voiceNext.GetConnection(context.Guild);
            if (voiceConnection != null)
            {
                voiceConnection.Disconnect();
            }
        }

        [Command("sr")]
        public async Task AddSongRequest(CommandContext context, string request)
        {
            if (!songRequestsContainers.ContainsKey(context.Guild.Id) || !songRequestsContainers[context.Guild.Id].IsConnected)
            {
                await context.RespondAsync("Sorry. Songrequests off :(");
                return;
            }
            int userRequestsInQueve = songRequestsContainers[context.Guild.Id].SongRequests.Where(x => x.RequestedBy == context.User.Username).Count();
            if (userRequestsInQueve >= songRequestsContainers[context.Guild.Id].LimitPerUser)
            {
                await context.RespondAsync($"Hey {context.User.Mention}! You have {userRequestsInQueve} requests in queve. Please wait");
                return;
            }
            string url = YouTubeLinkBuilder.BuildLink(request);
            string title = YoutubeDownloader.GetTitle(url);
            if(title == string.Empty)
            {
                await context.RespondAsync("Sorry, I can't find your song from request. Check if your link or video id is correct");
                return;
            }
            SongRequest newRequest = new SongRequest()
            {
                Link = url,
                Title = title,
                RequestedBy = context.User.Username
            };
            songRequestsContainers[context.Guild.Id].SongRequests.Add(newRequest);
            await context.RespondAsync($"{newRequest.Title} has been added by {newRequest.RequestedBy}");
            if (!songRequestsContainers[context.Guild.Id].IsPlayed)
            {
                songRequestsContainers[context.Guild.Id].IsPlayed = true;
                await Play(context);
            }
        }

        [Command("clearsr")]
        public async Task ClearSongRequest(CommandContext context)
        {
            if (UserPermittedToUse(context))
            {
                return;
            }
            if (!songRequestsContainers.ContainsKey(context.Guild.Id))
            {
                return;
            }
            songRequestsContainers[context.Guild.Id].SongRequests.Clear();
        }

        [Command("skipsr")]
        public async Task SkipSongRequest(CommandContext context)
        {
            if (!songRequestsContainers.ContainsKey(context.Guild.Id))
            {
                await context.RespondAsync($"{context.User.Mention} you don't have any songrequests");
                return;
            }
            var songRequest = songRequestsContainers[context.Guild.Id].SongRequests.Where(x => x.RequestedBy == context.User.Username).LastOrDefault();
            if (songRequest == null)
            {
                await context.RespondAsync($"{context.User.Mention} you don't have any songrequests");
                return;
            }
            songRequestsContainers[context.Guild.Id].SongRequests.Remove(songRequest);
            await context.RespondAsync($"{songRequest.Title} has been deleted from list.");
        }

        [Command("setsrlimit")]
        public async Task SetSongRequestsLimit(CommandContext context, int limit)
        {
            if (UserPermittedToUse(context))
            {
                return;
            }
            if (!songRequestsContainers.ContainsKey(context.Guild.Id))
            {
                await context.RespondAsync("Sorry! Songrequests off.");
                return;
            }
            if (limit <= 0)
            {
                await context.RespondAsync("Please enter number greater than 0");
                return;
            }
            songRequestsContainers[context.Guild.Id].LimitPerUser = limit;
            await context.RespondAsync($"Songrequests limit has been increased to {limit}");
        }
        #endregion

        #region Helpers
        private async Task Play(CommandContext context)
        {
            var voiceNext = context.Client.GetVoiceNextClient();
            var voiceConnection = voiceNext.GetConnection(context.Guild);
            if (voiceConnection == null)
            {
                await context.RespondAsync("Error! I am not joined to voice channel. Use command !join");
                return;
            }
            while (songRequestsContainers[context.Guild.Id].SongRequests.Count != 0)
            {
                var songRequest = songRequestsContainers[context.Guild.Id].SongRequests[0];
                songRequestsContainers[context.Guild.Id].SongRequests.RemoveAt(0);
                string filepath = await YoutubeDownloader.DownloadFile(songRequest.Link);
                if (filepath == "error" || filepath == string.Empty)
                {
                    await context.RespondAsync($"Problem with download {songRequest.Title}. Request will be skipped. Sorry {songRequest.RequestedBy} :(");
                    continue;
                }
                if (!File.Exists(Constants.PathToTmpFile))
                {
                    await context.RespondAsync($"Problem with download {songRequest.Title}. Request will be skipped. Sorry {songRequest.RequestedBy} :(");
                    continue;
                }
                await voiceConnection.SendSpeakingAsync(true);
                var ffmpegProcess = new ProcessStartInfo()
                {
                    FileName = Constants.PathToFFMpeg,
                    Arguments = $@"-i ""{filepath}"" -ac 2 -f s16le -ar 48000 pipe:1",
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                };
                try
                {
                    var ffmpeg = Process.Start(ffmpegProcess);
                    var ffout = ffmpeg.StandardOutput.BaseStream;

                    var buff = new byte[3840];
                    var bytes = 0;
                    while ((bytes = ffout.Read(buff, 0, buff.Length)) > 0)
                    {
                        if (bytes < buff.Length) // not a full sample, mute the rest
                            for (var i = bytes; i < buff.Length; i++)
                                buff[i] = 0;

                        await voiceConnection.SendAsync(buff, 20);
                    }
                    await voiceConnection.SendSpeakingAsync(false);
                    File.Delete(filepath);
                }
                catch
                {
                    await context.RespondAsync("Problem with playing music. Process stopped with error.");
                }
            }
            songRequestsContainers[context.Guild.Id].IsPlayed = false;
        }
        #endregion
    }
}
