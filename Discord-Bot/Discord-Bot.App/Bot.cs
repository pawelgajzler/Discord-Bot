﻿using Discord_Bot.App.Helpers;
using Discord_Bot.Configuration;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.VoiceNext;
using System;
using System.Threading.Tasks;

namespace Discord_Bot.App
{
    public class Bot
    {
        private DiscordClient discordClient;
        private CommandsNextModule commands;
        private VoiceNextClient voiceNext;

        public Bot()
        {
            discordClient = new DiscordClient(new DiscordConfiguration()
            {
                Token = ConfigurationManager.Instance.Config.BotToken,
                TokenType = TokenType.Bot
            });
            ConfigureMessages();
            ConfigureCommands();
        }

        private void ConfigureCommands()
        {
            commands = discordClient.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefix="!",
                EnableDms = false
            });
            voiceNext = discordClient.UseVoiceNext();
            foreach(Type moduleType in CommandModulesLocator.GetActiveModules())
            {
                if (moduleType.IsClass && !moduleType.IsAbstract)
                {
                    commands.RegisterCommands(moduleType);
                }
            }
        }

        private void ConfigureMessages() => discordClient.MessageCreated += DiscordClient_MessageCreated;

        private async Task DiscordClient_MessageCreated(DSharpPlus.EventArgs.MessageCreateEventArgs e)
        {
        }

        public async Task Start()
        {
            await discordClient.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}
