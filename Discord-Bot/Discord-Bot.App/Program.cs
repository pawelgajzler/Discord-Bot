﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Discord_Bot.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Bot discordBot = new Bot();
            discordBot.Start();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
