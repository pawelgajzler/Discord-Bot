﻿using Discord_Bot.CommonTypes;
using System.Text.RegularExpressions;

namespace Discord_Bot.App.Helpers
{
    public class YouTubeLinkBuilder
    {
        private static Regex fullLinkRegex = new Regex("www.youtube[\\.]{1}com/watch[\\?]{1}v=[A-Za-z0-9]+");

        private static Regex linkWithOutW = new Regex("youtube[\\.]{1}com/watch[\\?]{1}v=[A-Za-z0-9]+");

        private static Regex shortLinkRegex = new Regex("youtu[\\.]{1}be/[A-Za-z0-9]+");

        private static Regex videoIdWithV = new Regex("^v=[A-Za-z0-9]+");

        public static string BuildLink(string input)
        {
            if (fullLinkRegex.Match(input).Success || shortLinkRegex.Match(input).Success || linkWithOutW.Match(input).Success)
            {
                return (input.Contains("http://") || input.Contains("https://")) ? input : $"https://{input}";
            }
            if (videoIdWithV.Match(input).Success)
            {
                return $"{Constants.YoutubeUrl}{input}";
            }
            else
            {
                return $"{Constants.YoutubeUrl}v={input}";
            }
        }
    }
}
