﻿using Discord_Bot.App.CommandModules;
using Discord_Bot.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Discord_Bot.App.Helpers
{
    public static class CommandModulesLocator
    {
        public static List<Type> GetAllModules()
        {
            var interfaceType = typeof(BaseModule);
            var modules = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => interfaceType.IsAssignableFrom(p))
                .ToList();
            return modules;
        }

        public static List<Type> GetActiveModules()
        {
            var modules = GetAllModules().Where(x => ConfigurationManager.Instance.Config.ModuleList.Contains(x.Name));
            return GetAllModules();
        }
    }
}
