﻿using Discord_Bot.CommonTypes;
using VideoLibrary;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace Discord_Bot.App.Helpers
{
    public class YoutubeDownloader
    {
        public static async Task<string> DownloadFile(string link)
        {
            string filePath = Path.GetTempFileName();
            var youtubeDL = YouTube.Default;
            var video = await youtubeDL.GetVideoAsync(link);
            if (video == null)
            {
                return "error";
            }
            var bytes = await video.GetBytesAsync();
            File.WriteAllBytes(filePath, bytes);
            return filePath;
        }

        public static string GetTitle(string url)
        {
            var api = $"http://youtube.com/get_video_info?video_id={GetArgs(url, "v", '?')}";
            return GetArgs(new WebClient().DownloadString(api), "title", '&');
        }

        private static string GetArgs(string args, string key, char query)
        {
            var iqs = args.IndexOf(query);
            if (iqs == -1)
            {
                return string.Empty;
            }
            var title = HttpUtility.ParseQueryString(iqs < args.Length - 1 ? args.Substring(iqs + 1) : string.Empty)[key];
            if (title == null)
            {
                return string.Empty;
            }
            return title;
        }
    }
}
